require 'nokogiri'
require 'httparty'
require 'byebug'

def web_scraper
	continuer = "oui"
	puts "Récupération des missions..."
	job_titre = "placeholder"
	job_info_2 = "placeholder"
	url = 'https://www.service-civique.gouv.fr/missions/?criteria%5Bis_overseas%5D=0&criteria%5Bdepartments%5D%5B0%5D=' + $departement + '&page=' + $page.to_s
	unparsed_http = HTTParty.get(url)
	parsed_http = Nokogiri::HTML(unparsed_http.body)
	jobs = parsed_http.css('article.mission')
	for job in jobs
		numero = jobs.index(job) + 1
		a = $page
		while a > 1
			numero = numero + 12
			a = a - 1
		end
		puts "-----"
		puts "Job numéro " + numero.to_s
		job_titre = job.css('h1.h3').text
		job_info_1 = job.css('section.informations').text
		job_info_2 = job_info_1.slice(job_info_1.index('Où')..job_info_1.index('Voir plus') - 16)
		job_url_1 = job.css('a.more_link').to_s
		job_url_2 = job_url_1.slice(job_url_1.index('/missions')..job_url_1.index('title') - 3)
		puts job_titre
		puts job_info_2
		print 'URL: https://www.service-civique.gouv.fr'
		puts job_url_2
		puts "-----"
		puts "Voulez-vous accéder aux détails de cette mission ? (oui)"
		det = gets.chomp
		if det == "oui"
			puts "-----"
			nouvelle_url = 'https://www.service-civique.gouv.fr' + job_url_2
			unparsed_nouvelle = HTTParty.get(nouvelle_url)
			parsed_nouvelle = Nokogiri::HTML(unparsed_nouvelle.body)
			details = parsed_nouvelle.css('section.mission-description').text
			puts details
			puts "Vous pouvez envoyer une candidature ici: " + nouvelle_url + "/candidature"
			puts "-----"
			puts "Voulez-vous importer le texte ci-dessus dans un fichier texte ? (oui)"
			importer = gets.chomp
			if importer == "oui"
				fichier = File.new("(" + $departement + ") Job numéro " + numero.to_s + '.txt', 'w')
				fichier.write(job_titre)
				fichier.write(details)
				fichier.write(nouvelle_url)
				fichier.close
				puts "Fichier texte crée dans le dossier du programme."
			else
				puts "Aucun fichier n'a été crée."
			end
			puts "-----"
			puts "Voulez-vous voir les prochaines missions ? (oui)"
			continuer = gets.chomp
			break if continuer != "oui"
		end
	end
	if continuer != "oui"
		return false
	end
	if jobs.count >= 12
		next_page
	else
		puts "Il n'y a pas d'autres jobs disponibles dans le " + $departement + ". Merci d'avoir utilisé ce programme !"
		puts "Est-ce que vous voulez changer de département ? (oui)"
		response = gets.chomp
		if response == "oui"
			quel_departement
		else
			puts "Au revoir ! o/"
		end
	end
	rescue SocketError => error_message
		puts "Le programme n'a pas pu se connecter au site web. Vérifiez votre connexion."
		puts "\nPour référence, voici le message d'erreur:\n" + error_message.to_s
end

def next_page
	puts "Page suivante ? (oui/non/departement)"
	reponse = gets.chomp
	if reponse == "oui"
		$page = $page + 1
		puts "Voici la page " + $page.to_s
		web_scraper
	elsif reponse == "non"
		puts "Au revoir !"
	elsif reponse == "departement"
		puts "Changement de département..."
		puts "-----"
		quel_departement
	else
		puts "Réponse invalide..."
		next_page
	end
end

def quel_departement
	puts 'Quel département ?'
	$departement = gets.chomp.delete(' ')
	if $departement[0] == '0' #Prevents Integer conversion to return false if departement is 08 or 09
		$departement[0] = ''
	end
	result = Integer($departement) rescue false
	if result.is_a? Integer
		if $departement.length == 1 #All "departements" are double-digits, so put a 0 as first character if only one digit is returned
			$departement.insert(0, '0')
		end
		$page = 1
		web_scraper
	else
		puts "Il me faut un nombre."
		quel_departement
	end
end

quel_departement
