Ce programme accède au site web https://www.service-civique.gouv.fr et vous donne les missions qui y sont proposées directement dans votre console de commandes.

Vous pouvez consulter les détails de chaque mission qui vous est proposée.

Lorsque le programme vous demande un département, c'est le numéro du département qui est demandé. 75 pour Paris, par example.


**Added in v1.1:**

*  People can now check a mission's details within the console if they wish to
*  Missions are now shown 1 by 1 instead of all missions of one page at once


**Added in v1.2:**

*  People can now print into a textfile any mission's details and URL
*  Fixed a bug which caused 08 and 09 to not be considered as Integer when asked for "departement"
*  You're no longer forced to add a 0 as first character when asked for departement to get results if said department is from (0)1 to (0)9


**Added in v1.3:**

*  HTTParty's deprecation warning will no longer show up
*  White spaces will now be ignored when asked for "departement"
*  File name now features more details
*  Text will now actually show up if you choose not to print a job's details into a text file
